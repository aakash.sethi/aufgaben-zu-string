package com.company;

public class StringClass {
    public StringClass() {
        String str1 = "Aakash";
        String str2 = "Sethi";
        System.out.println(str1.endsWith(str2));
        String str3 = "wohnung";
        String str4 = ".jpg";
        System.out.println(str3.endsWith(str4));
        String str5 = "hans.muster@bbw.ch";
        System.out.println(str5.endsWith("@bbw.ch"));
        String str6 = "small-skateboard.jpg";
        System.out.println(str6.startsWith("small"));


    }
}
