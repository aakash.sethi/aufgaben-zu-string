package com.company;
import java.util.Scanner;
public class StringClass {
    public StringClass() {
        Scanner keyboard = new Scanner(System.in);
        String input = keyboard.nextLine();
        System.out.println("Der Text ist " + input.length() + "Zeichen lang");

        if (input.length() % 2 == 0) {
            System.out.println("Die Anzahl Zeichen ist gerade.");
        }
        else {
            System.out.println("Die Anzahl Zeichen ist ungerade");
        }
        System.out.println("Erster Buchstabe: " + input.charAt(0));
        System.out.println("Letzterv Buchstabe: " + input.charAt(input.length()-1));
    }
}
