package com.company;

import java.util.Scanner;

public class StringClass {
    public StringClass() {
        Scanner keyboard = new Scanner(System.in);
        String str1 = keyboard.nextLine();
        if (str1.contains(".jpg") || str1.contains(".JPG")) {
            System.out.println("File contains an image.");
        }
        else {
            System.out.println("File does not contain an image.");
        }
    }
}
