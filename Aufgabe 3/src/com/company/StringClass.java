package com.company;

public class StringClass {
    public StringClass() {
        String stu1 = "Hans";
        String stu2 = "Paula";
        System.out.println(stu1.compareTo(stu2));
        String stu3 = "Patrick";
        String stu4 = "Anna";
        System.out.println(stu3.compareTo(stu4));
        String stu5 = "Silvan";
        String stu6 = "Simon";
        System.out.println(stu5.compareTo(stu6));
    }
}
